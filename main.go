package main

import (
	"github.com/gofiber/fiber/v2"
	"go_sso.configs/env"
	"go_sso.configs/mysql"
)

func main() {
	env.LoadEnv()
	mysql.ConnectDB()

	app := fiber.New()

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, world")
	})

	app.Listen(":5000")
}
