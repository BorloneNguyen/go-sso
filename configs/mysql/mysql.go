package mysql

import (
	"log"
	"os"
	"strings"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func ConnectDB() (db *gorm.DB) {
	dbUser := os.Getenv("DB_USERNAME")
	dbPass := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")

	dsnSlice := []string{
		dbUser, ":",
		dbPass, "@tcp(",
		dbHost, ":",
		dbPort, ")/",
		dbName, "?ssl-mode=REQUIRED&sslcert=../certificates/ca.pem",
	}

	// rootCertPool := x509.NewCertPool()
	// pem, err := os.ReadFile("../../certificate/ca.pem")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// ok := rootCertPool.AppendCertsFromPEM(pem)
	// if !ok {
	// 	log.Fatal("Failed to append PEM.")
	// }
	// clientCert := make([]tls.Certificate, 0, 1)
	// certs, err := tls.LoadX509KeyPair("/path/mysqlClientCert", "/path/mysqlClientKey")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// clientCert = append(clientCert, certs)
	// mysql.RegisterTLSConfig("custom", &tls.Config{
	// 	RootCAs:      rootCertPool,
	// 	Certificates: clientCert,
	// })

	// refer https://github.com/go-sql-driver/mysql#dsn-data-source-name for details
	dsn := strings.Join(dsnSlice, "")
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Error to connect to database")
	}

	return db
}
